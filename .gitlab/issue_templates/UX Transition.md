## Context

This issue is dedicated to the seamless transition of UX and Design responsibilities for `_ STAGE GROUP, GROUP, OR CATEGORY_` from `_YOUR HANDLE_` to `_NEWLY ASSIGNED DESIGNER HANDLE_`.

The designer currently assigned to the stage group is responsible for providing the relevant content for the newly assigned designer. However, it is encouraged to partner closely with Product Management, Design leadership, and stage group counterparts to provide the necessary information for a seamless transition.

<!-- Add any additional high-level context you think would be helpful here. -->

## :ballot_box_with_check: Tasks

The below tasks should be completed before finalizing the transition and closing this issue.

* [ ] Update the title of this issue to: `UX Transition for _ STAGE GROUP, GROUP, CATEGORY OR FEATURE_`.
* [ ] Assign the transition issue to yourself and your Product Design Manager.
    * [ ] Optionally, add a link to the MR confirming the transition [ADD LINK]()
* [ ] Fill out the sections below with the relevant content for the stage, group, or category.
* [ ] Inform relevant Product Managers and Engineering Managers of the UX transition by pinging them in this issue.
    * [ ] Optionally, chedule a sync meeting with the newly assigned Product Designer, Product Design Manager, and Product Manager. You can also opt for recording a walkthrough video to share with the relevant team.
* [ ] If a category or feature is being moved, [transfer the JTBDs](https://about.gitlab.com/handbook/engineering/ux/category-maturity-scorecards/#update-jtbd-data-file) to the relevant team by updating them in the handbook.
* [ ] Communicate the change with the stage groups on Slack by sharing a link to this issue.
* [ ] **Newly Assigned Designer**: When you feel comfortable, unassign the previous designer and close this issue.

## :hourglass: History and timeline

<!-- Add any information here that would be helpful to understand **why** this transition is happening. Make sure to link to any relevant issues, MRs, or discussions for context. -->

#### Timeline for the transition

| Milestone | Description |
| --- | --- |
| `00.00` | `Add a description` |

## :eyeglasses: Product vision

* UX Team handbook page: [ADD LINK]()
* Stage, group, or category handbook pages: [ADD LINK]()
* Jobs to be Done (JTBD) handbook page: [ADD LINK]()
* Stage or Category Vision: [ADD LINK]()
* Section UX Strategy: [ADD LINK]()
* List the Category or Feature labels associated to this transition: `~LABEL` `~LABEL`

* Additional resources: 
  * Add resource: [ADD LINK]()

## :construction_worker: Current and upcoming initiatives

<!-- List the current priorities and design needs for the work being transitioned. Include relevant links such as Figma files, Dovetail insights, and any other resources. --> 

| Priority | Milestone | Current status | Issue/MR link | Design resource |
|---------- | ----------------- | -------------------- | ------------------ | ----------- |
| `_PRIORITY_` | `_MILESTONE_` | `_WORKFLOW LABEL_` | [Add issue/MR link](ADD LINK) | [Add design source](ADD LINK) |


#### Parked tasks

<!-- Add any additional work that you planned to get to, but isn't active at the moment. Parked tasks will remain and the new designer should follow up on the priority/scope with PM and other counterparts. -->

## :calendar_spiral: Meetings and rituals

<!-- List out the relevant meetings, rituals, or slackbots the new designer should be added to. Then add the person who can add them to the ritual. -->

- [ ] [Event name](ADD LINK) - `@mention`
